package Application;

import Application.GUI.*;
import javax.swing.*;
import java.io.IOException;

public class Tests {
    public static void main(String[] args) throws IOException, ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {

        UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        MainFrame frame = new MainFrame();

    }
}