package Application;

import java.util.ArrayList;
import java.util.List;

public class Transfer {
    public static List<Tender> stringToTender(List<String> lines){

        if ((lines == null) || (lines.isEmpty())){
            return null;
        }

        List<Tender> tenders = new ArrayList<Tender>();
        for (int i = 0; i < lines.size(); i++){
            String[] words = lines.get(i).split(",");
            if (words.length != 4){
                return null;
            }
            Tender tender = new Tender();
            tender.setCodeOfTheCompany(words[0].trim());
            tender.setEnterpriseCode(words[1].trim());
            tender.setNameOfTheService(words[2].trim());
            try {
                tender.setProposedAmount(Double.parseDouble(words[3].trim()));
            }
            catch (NumberFormatException e){
                return null;
            }
            tenders.add(tender);
        }
        return tenders;
    }

    public static List<String> tendersToString(List<Tender> tenders){
        if ((tenders == null) || (tenders.isEmpty())){
            return null;
        }
        List<String> lines = new ArrayList<String>();
        for (int i = 0; i < tenders.size(); i++){
            lines.add(String.format("%10s, %10s, %15s, %5.3f", tenders.get(i).getCodeOfTheCompany(), tenders.get(i).getEnterpriseCode(), tenders.get(i).getNameOfTheService(), tenders.get(i).getProposedAmount()));
        }
        return lines;
    }

}
