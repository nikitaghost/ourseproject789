package Application;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class IO {
    public static List<String> inputLines(String fileName) throws IOException {
        List<String> lines = new ArrayList<String>();
        String line;
        BufferedReader input = null;
        try {
            input = new BufferedReader(new FileReader(fileName));
            while ((line = input.readLine()) != null){
                line = line.trim();
                if (line.equals("")) continue;
                lines.add(line);
            }
        }
        catch (IOException e){
            return null;
        }
        finally {
            if (input != null){
                input.close();
            }
            if (lines.isEmpty()) {
                return null;
            }
        }
        return lines;
    }

    public static boolean outputLines(String file, List<String> lines) throws IOException{

        PrintWriter out = null;
        if ((lines == null) || (lines.isEmpty())){
            return false;
        }
        try{
             out = new PrintWriter(new FileWriter(file));
             for (int i = 0; i < lines.size(); i++){
                 out.println(lines.get(i).trim());
             }
        }
        catch (IOException e){
            return false;
        }
        finally {
            if (out != null){
                out.close();
            }
            return true;
        }
    }
}
