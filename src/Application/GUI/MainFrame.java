package Application.GUI;

import Application.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainFrame extends JFrame implements ActionListener
{
    public static JFrame jFrame;
    static JTable jTable;
    MenuBar menuBar;
    JPanel panel;
    static JLabel msg;
    String directoryName = "C:/";
    String fileName = "";
    File curFile;
    List<String> lines = null;
    JLabel jFileName;
    ViewPanel viewPanel;
    EditPanel editPanel;

    static String  developer ="\n     Систему разработал студент группы ИВТ/б-19-2-о\n "+ "    Китайчик Никита Дмитриевич:\n"+ "     СевГУ - 2020.\n";
    static String about = "\n     Информационная система осуществляет хранение и\n"+"     Обработку данных о заявках компаний\n"+"     в тендере.\n";

    public MainFrame() throws IOException {
        jFrame = new JFrame("Меню");

        //Work with table
        Global.table = new TenderGroup("Тендер на оказание ремонтных услуг муниципальным предприятиям");
        Global.tenders = new ArrayList<Tender>();
        Global.tableModel = new TenderTableModel(Global.tenders);
        jTable = new JTable(Global.tableModel);
        jTable.setRowHeight(25);
        jTable.setCursor(new Cursor(Cursor.TEXT_CURSOR));
        jTable.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
        jTable.setGridColor(new Color(204,204,204));
        jTable.setSelectionForeground(new Color(0,0,0));
        jTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jTable.setAutoCreateRowSorter(true);
        jTable.setFillsViewportHeight(true);
        jTable.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

                try {
                    TenderTableModel tenderTableModel = (TenderTableModel) jTable.getModel();

                    String codeOfTheCompany = tenderTableModel.getValueAt(jTable.getSelectedRow(), 0).toString();
                    String enterpriseCode = tenderTableModel.getValueAt(jTable.getSelectedRow(), 1).toString();
                    String nameOfTheService = tenderTableModel.getValueAt(jTable.getSelectedRow(), 2).toString();
                    String proposedAmount = tenderTableModel.getValueAt(jTable.getSelectedRow(), 3).toString();

                    EditPanel.textFieldCode.setText(codeOfTheCompany);
                    EditPanel.textFieldCipher.setText(enterpriseCode);
                    EditPanel.textFieldNameOfTheService.setText(nameOfTheService);
                    EditPanel.textFieldAmount.setText(proposedAmount);
                }
                catch (IndexOutOfBoundsException i){

                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        JScrollPane scrtable = new JScrollPane(jTable);
        jTable.setPreferredScrollableViewportSize(new Dimension(250,100));

        viewPanel = new ViewPanel();
        editPanel = new EditPanel();

        menuBar = new MenuBar();
        jFrame.setJMenuBar(menuBar.jMenuBar);

        menuBar.newItem.addActionListener(this);
        menuBar.openItem.addActionListener(this);
        menuBar.saveItem.addActionListener(this);
        menuBar.saveAsItem.addActionListener(this);
        menuBar.closeItem.addActionListener(this);
        menuBar.startEditing.addActionListener(this);
        menuBar.endEditing.addActionListener(this);
        menuBar.about.addActionListener(this);
        menuBar.description.addActionListener(this);

        int width = 800;
        int height = 600;

        Container container = jFrame.getContentPane();
        container.setLayout(new BorderLayout(5,5));

        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(scrtable, BorderLayout.CENTER);
        panel.add(editPanel, BorderLayout.SOUTH);
        jFileName = new JLabel("Без имени", JLabel.CENTER);
        panel.add(jFileName, BorderLayout.NORTH);

        int v = ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
        int h = ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
        JScrollPane scrollPane = new JScrollPane(panel, v, h);
        container.add(scrollPane, BorderLayout.CENTER);
        container.add(new JLabel(Global.table.getName(), JLabel.CENTER), BorderLayout.NORTH);
        msg = new JLabel("Курсовой проект по дисциплине \"Программирование\". СевГУ - 2020 ");
        msg.setFont(new Font("Serif", Font.BOLD, 14));
        container.add(msg, BorderLayout.SOUTH);
        jFrame.setSize(width, height);
        jFrame.setLocation(100,100);
        jFrame.setVisible(true);

    }

    public void newItem(){

        Global.table.getTenders().clear();
        Global.tenders.clear();
        Global.tableModel.fireTableDataChanged();
        msg.setText("Создание базы данных");

    }

    public void openItem(){

        int res;
        int index;

        msg.setText("Открытие файла");
        JFileChooser fileOpen = new JFileChooser();
        fileOpen.setDialogTitle("Открытие файла");
        setFileFilter(fileOpen);
        res = fileOpen.showDialog( jFrame, "Open");

        if (res == fileOpen.APPROVE_OPTION){

            curFile = fileOpen.getSelectedFile();
            fileName = curFile.getAbsolutePath();
            index = fileName.lastIndexOf("\\");
            directoryName = fileName.substring(0, index + 1);

            try{

                lines = IO.inputLines(fileName);

            } catch (Exception e){
                msg.setText("Ошибка ввода данных");
            }

            List<Tender> tenders = Transfer.stringToTender(lines);
            Global.table.getTenders().clear();
            Global.table.setTenders(tenders);
            Global.updateJTable(Global.table.getTenders());
            jFrame.repaint();
            jFrame.pack();
            jFrame.setSize(800, 600);
            jFileName.setText(fileName);

        }
    }

    public void setFileFilter( JFileChooser fileChooser ){

        TextFilter textFilter = new TextFilter();
        fileChooser.setFileFilter(textFilter);

    }

    public void saveDialog(){

        int res;
        int index;

        JFileChooser fileSave = new JFileChooser(directoryName);
        fileSave.setDialogTitle("Сохранение файла");
        setFileFilter(fileSave);
        res = fileSave.showDialog((Component) jFrame, "Save");
        if (res == fileSave.APPROVE_OPTION){
            curFile = fileSave.getSelectedFile();
            fileName = curFile.getAbsolutePath();
            index = fileName.lastIndexOf("\\");
            directoryName = fileName.substring(0, index + 1);

        }
    }

    public void saveItem(boolean fs){

        String oldFileName = fileName;
        msg.setText("Сохранение файла");
        if (fs){
            saveDialog();
        }
        else if (fileName == ""){
            saveDialog();
        }
        if (curFile == null){
            msg.setText("Файл для сохранения не выбран");
            return;
        }
        if ((!curFile.exists()) || fileName.equals(oldFileName)){
            lines = Transfer.tendersToString(Global.table.getTenders());
            try{
                boolean f = IO.outputLines(fileName, lines);
                if (f){
                    msg.setText("Данные успешно сохранены");
                    jFileName.setText(fileName);
                }
                else {
                    msg.setText("Ошибка сохранения данных");
                }
            } catch (Exception e) {
                msg.setText("Ошибка сохранения данных");
            }
        }
        else{
            JOptionPane.showMessageDialog(jFrame, "Ошибка: файл с заданным именем " + fileName + " существует");
            fileName = oldFileName;
        }
        msg.setText("Курсовой проект по дисциплине \"Программирование\". СевГУ - 2016");
    }

    public void closeItem(){
        jFrame.dispose();
    }

    public void startEditing(){

        panel.remove(viewPanel);
        panel.add(editPanel, BorderLayout.SOUTH);
        jFrame.repaint();
        jFrame.pack();
        jFrame.setSize(800, 600);

    }

    public void endEditing(){

        panel.remove(editPanel);
        panel.add(viewPanel, BorderLayout.SOUTH);
        jFrame.repaint();
        jFrame.pack();
        jFrame.setSize(800, 600);

    }
    
    @Override
    public void actionPerformed(ActionEvent e) {

        if ("Новый".equals(e.getActionCommand())) {
            newItem();
        } else if ("Открыть".equals(e.getActionCommand())) {
            openItem();
        } else if ("Закрыть".equals(e.getActionCommand())) {
            closeItem();
        } else if ("Сохранить".equals(e.getActionCommand())) {
            saveItem(false);
        } else if ("Сохранить как".equals(e.getActionCommand())) {
            saveItem(true);
        } else if ("Начать редактирование".equals(e.getActionCommand())){
            startEditing();
        } else if ("Закончить редактирование".equals(e.getActionCommand())){
            endEditing();
        } else if("О программе".equals(e.getActionCommand())){
            HelpDialog helpDialog = new HelpDialog(MainFrame.jFrame,"О программе", developer, "cat.gif");
            helpDialog.setVisible(true);
        } else if ("Описание ИС".equals(e.getActionCommand())){
            HelpDialog helpDialog = new HelpDialog(MainFrame.jFrame, "Описание Информационной системы", about, "cat.gif");
            helpDialog.setVisible(true);
        }
        
    }
}