package Application;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class TotalTableModel extends AbstractTableModel {

    List<TotalRecord> records;

    public TotalTableModel(List<TotalRecord> records){

        this.records = records;

    }

    @Override
    public int getRowCount(){

        return records.size();

    }

    @Override
    public int getColumnCount(){

        return 3;

    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        TotalRecord totalRecord = records.get(rowIndex);

        switch (columnIndex){

            case 0:
                return totalRecord.getCodeOfTheCompany();
            case 1:
                return totalRecord.getNameOfTheService();
            case 2:
                return totalRecord.getMinProposedAmount();

        }

        return "";

    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex){
            case 0:
                return "Код компании";
            case 1:
                return "Название работ";
            case 2:
                return "Минимальная сумма";

        }
        return "";
    }

}
