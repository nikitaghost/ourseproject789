package Application.GUI;

import Application.Global;
import Application.Tender;
import Application.TenderTableModel;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.List;


public class EditPanel extends JPanel {

    static JTextField textFieldCode;
    static JTextField textFieldCipher;
    static JTextField textFieldAmount;
    static JTextField textFieldNameOfTheService;
    static JTextField textFieldWriteAmount;

    public EditPanel() {
        setLayout(new GridLayout(3, 4, 2, 2));
        JButton buttonAdd = new JButton("Добавить");
        JButton buttonUpdate = new JButton("Обновить");
        JButton buttonDelete = new JButton("Удалить");
        JButton buttonDeleteGroup = new JButton("Удалить группу записей:");
        textFieldCode = new JTextField("");
        textFieldCipher = new JTextField("");
        textFieldNameOfTheService = new JTextField("");
        textFieldAmount = new JTextField("");
        textFieldWriteAmount = new JTextField("");

        JLabel l1 = new JLabel("");
        JLabel l2 = new JLabel("               Введите сумму для удаления:");

        add(textFieldCode);
        add(textFieldCipher);
        add(textFieldNameOfTheService);
        add(textFieldAmount);
        add(buttonAdd);
        add(buttonUpdate);
        add(buttonDelete);
        add(l1);
        add(buttonDeleteGroup);
        add(l2);
        add(textFieldWriteAmount);

        buttonAdd.addActionListener(new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            insert();
                                        }
                                    }
        );

        buttonUpdate.addActionListener(new ActionListener() {
                                   public void actionPerformed (ActionEvent e) {
                                       update();
                                   }
                               }
        );

        buttonDelete.addActionListener(new ActionListener() {
                                   public void actionPerformed (ActionEvent e) {
                                       delete();
                                   }
                               }
        );

        buttonDeleteGroup.addActionListener(new ActionListener() {
                                   public void actionPerformed (ActionEvent e) {
                                       deleteGroup();
                                   }
                               }
        );


    }

    private void insert(){

        double amount;
        String strCode = textFieldCode.getText();
        String strCipher = textFieldCipher.getText();
        String strTitle = textFieldNameOfTheService.getText();
        String strAmount = textFieldAmount.getText();

        if ( strCode.equals("") || strCipher.equals("") || strTitle.equals("") || strAmount.equals("") ){
            MainFrame.msg.setText("Задайте значения полей");
            return;
        }
        try{
            amount = Double.parseDouble(strAmount);
        }
        catch (NumberFormatException  e){
            MainFrame.msg.setText("   Задайте правильную предлагаемую сумму");
            return;
        }
        MainFrame.msg.setText(
                "   Запрос на добавление записи в таблицу");
        if (!Global.table.addTender(new Tender(strCode, strCipher, strTitle, amount)))
            MainFrame.msg.setText(
                    "   Запись не добавлена, возможно нарушена уникальность ключа");
        Global.updateJTable(Global.table.getTenders());
        textFieldCode.setText("");
        textFieldCipher.setText("");
        textFieldNameOfTheService.setText("");
        textFieldAmount.setText("");
        MainFrame.jFrame.repaint();
        MainFrame.jFrame.pack();
        MainFrame.jFrame.setSize(800, 600);

    }

    private void update(){

        TenderTableModel tenderTableModel = (TenderTableModel) MainFrame.jTable.getModel();

        if ( MainFrame.jTable.getSelectedRowCount() == 1 ){

            String codeOfTheCompany = textFieldCode.getText();
            String enterpriseCode = textFieldCipher.getText();
            String nameOfTheService = textFieldNameOfTheService.getText();
            String proposedAmount = textFieldAmount.getText();

            List<Tender> tendersList = Global.tenders;
            tendersList.get(MainFrame.jTable.getSelectedRow()).setCodeOfTheCompany(codeOfTheCompany);
            tendersList.get(MainFrame.jTable.getSelectedRow()).setEnterpriseCode(enterpriseCode);
            tendersList.get(MainFrame.jTable.getSelectedRow()).setNameOfTheService(nameOfTheService);
            tendersList.get(MainFrame.jTable.getSelectedRow()).setProposedAmount(Double.parseDouble(proposedAmount));

            Global.tenders = tendersList;
            Global.tableModel = new TenderTableModel(tendersList);

        } else{

            if ( MainFrame.jTable.getRowCount() == 0){

            } else{
                JOptionPane.showMessageDialog(this, "Пожалуйста, выделите только 1 строку для обновления!");
            }

        }

        MainFrame.jFrame.repaint();
        MainFrame.jFrame.pack();
        MainFrame.jFrame.setSize(800, 600);
        JOptionPane.showMessageDialog(this, "Успешное обновление!") ;

    }

    private void  delete(){

        String strCode=textFieldCode.getText();
        String strCipher=textFieldCipher.getText();
        if (strCode.equals("") || strCipher.equals("")){

            MainFrame.msg.setText("Задайте значения полей ключа");
            return;
        }
        MainFrame.msg.setText(
                "   Запрос на удаление записи по ключу");
        if (!Global.table.delTender(new Tender(strCode,strCipher,"",0.0))) {
            MainFrame.msg.setText("   Запись не удалена, возможно записи с таким ключом нет");
        }
        Global.updateJTable(Global.table.getTenders());
        MainFrame.jFrame.repaint();
        MainFrame.jFrame.pack();
        MainFrame.jFrame.setSize(800, 600);
        textFieldCode.setText("");
        textFieldCipher.setText("");
        textFieldNameOfTheService.setText("");
        textFieldAmount.setText("");

    }

    private void deleteGroup() {

        String amount = textFieldWriteAmount.getText();

        if (amount.equals("")){
            MainFrame.msg.setText("    Введите сумму для удаления!");
        }

        if (!Global.table.deleteByAmount(Double.parseDouble(amount))) {
            MainFrame.msg.setText("   Записи не удалены, возможно таких записей нет");
        }

        textFieldCode.setText("");
        textFieldCipher.setText("");
        textFieldNameOfTheService.setText("");
        textFieldAmount.setText("");
        textFieldNameOfTheService.setText("");
        MainFrame.jFrame.repaint();
        MainFrame.jFrame.pack();
        MainFrame.jFrame.setSize(800, 600);
    }

}