package Application;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class TenderTableModel extends AbstractTableModel {

    private List<Tender> tenders;
    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

    public TenderTableModel(){

        tenders = new ArrayList<Tender>();

    }

    public TenderTableModel(List<Tender> tenders ){

        this.tenders = tenders;

    }


    @Override
    public int getRowCount() {
        return tenders.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex){
            case 0:
                return "Код компании";
            case 1:
                return "Шифр заказчика";
            case 2:
                return "Название работ";
            case 3:
                return "Сумма";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return TenderGroup.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Tender tender = tenders.get(rowIndex);

        switch (columnIndex){
            case 0:
                return tender.getCodeOfTheCompany();
            case 1:
                return tender.getEnterpriseCode();
            case 2:
                return tender.getNameOfTheService();
            case 3:
                return tender.getProposedAmount();

        }

        return "";

    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    @Override
    public void addTableModelListener(TableModelListener l) {

        listeners.add(l);

    }

    @Override
    public void removeTableModelListener(TableModelListener l) {

        listeners.remove(l);

    }
}

