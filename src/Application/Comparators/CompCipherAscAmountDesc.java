package Application.Comparators;

import Application.Tender;

import java.util.Comparator;

public class CompCipherAscAmountDesc implements Comparator {

    @Override
    public int compare(Object ob1, Object ob2){

        Tender tender1 = (Tender) ob1;
        Tender tender2 = (Tender) ob2;

        if (tender1.getEnterpriseCode().compareTo(tender2.getEnterpriseCode()) < 0){
            return -1;
        }
        else if (tender1.getEnterpriseCode().compareTo(tender2.getEnterpriseCode()) > 0){
            return 1;
        }
        else if(tender1.getProposedAmount() < tender2.getProposedAmount()){
            return 1;
        }
        else if (tender1.getProposedAmount() == tender2.getProposedAmount()){
            return 0;
        }
        else {
            return -1;
        }

    }

}
