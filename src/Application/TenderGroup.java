package Application;

import Application.GUI.MainFrame;

import java.lang.reflect.Array;
import java.util.*;

public class TenderGroup {

    private String name;
    private List<Tender> tenders;

    public TenderGroup(){
        this.name = "";
        this.tenders = new ArrayList<Tender>();
    }

    public TenderGroup(String name){

        this.name = name;
        this.tenders = new ArrayList<Tender>();

    }

    public TenderGroup(String name, List<Tender> tenders) {
        this.name = name;
        this.tenders = tenders;
    }

    public boolean addTender(Tender tender){

        if (tenders.add(tender)){
            return true;
        }

        return false;

    }

    public boolean delTender(Tender tenderDel){

        for (Tender tender : tenders){
            if (tender.getCodeOfTheCompany().equals(tenderDel.getCodeOfTheCompany()) || tender.getEnterpriseCode().equals(tenderDel.getEnterpriseCode())){
                tenders.remove(tender);
                return true;
            }
        }

        return false;

    }

    public boolean delTenders(String codeOfTheCompany){

        int deleted = 0;

        for (Tender tender : tenders ){
            if (tender.getCodeOfTheCompany().equals(codeOfTheCompany)){
                tenders.remove(tender);
                deleted++;
            }
        }

        if (deleted != 0){
            return true;
        }

        return false;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Tender> getTenders() {
        return tenders;
    }

    public void setTenders(List<Tender> tenders) {
        this.tenders = tenders;
    }

    public double avgPrice(){

        double amount = 0.0;

        for ( Tender tender : tenders){

            amount += tender.getProposedAmount();

        }

        double avgPrice = amount / tenders.size();

        return avgPrice;

    }

    public TenderGroup aboveAvgPrice(){

        TenderGroup tenderGroup = new TenderGroup();
        double avgPrice = avgPrice();

        for (Tender tender : tenders){
            if (tender.getProposedAmount() > avgPrice){
                tenderGroup.addTender(tender);
            }
        }

        return tenderGroup;

    }

    public TenderGroup filterCompany(String filter)
    {

        TenderGroup tenderGroup = new TenderGroup();

        for (Tender tender : tenders){

            if (tender.getCodeOfTheCompany().contains(filter)){
                tenderGroup.addTender(tender);
            }

        }

        return tenderGroup;

    }

    public boolean deleteByAmount( double amount ){

        int deleted = 0;

        for (Tender tender : tenders){
            if (tender.getProposedAmount() == amount){
                Global.tenders.remove(tender);
                deleted++;
            }
        }

        Global.table.setTenders(Global.tenders);
        MainFrame.jFrame.repaint();
        MainFrame.jFrame.pack();
        MainFrame.jFrame.setSize(800, 600);
        return deleted != 0;

    }

    public TenderGroup betweenPrice(double a, double b){

        TenderGroup tenderGroup = new TenderGroup();
        double first, second;

        if ( a <= b ){
            first = a;
            second = b;
        } else {
            first = b;
            second = a;
        }


        for ( Tender tender : tenders ){
            if ( (tender.getProposedAmount() >= first) && (tender.getProposedAmount() <= second) ){
                tenderGroup.addTender(tender);
            }
        }

        return tenderGroup;

    }

    public TenderGroup sort(){

        TenderGroup tenderGroup = new TenderGroup(name, tenders);
        Collections.sort(tenderGroup.tenders);
        return tenderGroup;

    }

    public TenderGroup sort(Comparator comparator){

        TenderGroup tenderGroup = new TenderGroup(name, tenders);
        Collections.sort(tenderGroup.tenders, comparator);
        return tenderGroup;

    }

    public List<TotalRecord> getMinAmounts(){

        List<TotalRecord> totalRecordList = new ArrayList<TotalRecord>();

        Set<String> namesOfTheServicesS = new TreeSet<String>();
        for (Tender tender : tenders){

            namesOfTheServicesS.add(tender.getNameOfTheService());

        }

        List<String> namesOfTheServices = new ArrayList<>(namesOfTheServicesS);
        List<Tender> tenderList = new ArrayList<>();

        for (int i = 0; i < namesOfTheServices.size(); i++){
            for (Tender tender : tenders){
                if (tender.getNameOfTheService().equals(namesOfTheServices.get(i))){
                    tenderList.add(tender);
                    break;
                }
            }
        }

        for (int i = 0; i < namesOfTheServices.size(); i++ ){

            for (Tender tender : tenders){

                if (tender.getNameOfTheService().equals(namesOfTheServices.get(i))){

                    if (tender.getProposedAmount() >= tenderList.get(i).getProposedAmount()){

                        tenderList.set(i, tender);

                    }

                }

            }

        }

        for (Tender tender : tenderList){

            TotalRecord totalRecord = new TotalRecord(tender.getCodeOfTheCompany(), tender.getNameOfTheService(), tender.getProposedAmount());
            totalRecordList.add(totalRecord);

        }

        System.out.println(namesOfTheServices);

        return totalRecordList;

    }

    public void print(){
        System.out.println(name);
        for (Tender tender : tenders){
            System.out.println(tender);
        }
    }

}
