package Application;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class TextFilter extends FileFilter {

    public boolean accept(File f){
        if (f.isDirectory()){
            return true;
        }

        String extension = getExtension(f);

        if (extension != null){
            if (extension.equals("txt") || extension.equals("bd")){
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }

    public String getDescription(){
        return "Текстовые файлы";
    }

    private static String getExtension(File f){

        String extension = null;
        String str = f.getName();
        int i = str.lastIndexOf(".");

        if ( (i > 0) && (i < str.length() - 1) ){
            extension = str.substring(i + 1).toLowerCase();
        }

        return extension;

    }

}
