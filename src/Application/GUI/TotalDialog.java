package Application.GUI;

import Application.Tender;
import Application.TenderTableModel;
import Application.TotalRecord;
import Application.TotalTableModel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class TotalDialog extends JDialog implements ActionListener {

    public TotalDialog(JFrame parent, List<TotalRecord> totalRecordList){

        super (parent, "Подведение итогов", true);
        Container container = getContentPane();
        container.setLayout(new BorderLayout());
        container.add (new JLabel("Заявки с минимальной суммой", JLabel.CENTER),BorderLayout.NORTH);
        JButton btnOk= new JButton ("ОК");
        btnOk.addActionListener(this);
        container.add(btnOk, BorderLayout.SOUTH);

        TotalTableModel tableModel = new TotalTableModel(totalRecordList);
        JTable jtable = new JTable(tableModel);
        jtable.setRowHeight(25);
        JScrollPane scrtable = new JScrollPane(jtable);
        jtable.setPreferredScrollableViewportSize(new Dimension(150, 150));
        add(scrtable,BorderLayout.CENTER);
        MainFrame.msg.setText("   Итоговый запрос на выборку");
        setSize(500,250);
        setLocation(50,100);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        dispose();

    }
}
