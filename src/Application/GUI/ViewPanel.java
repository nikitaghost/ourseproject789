package Application.GUI;

import Application.Comparators.CompCipherAscAmountDesc;
import Application.Comparators.CompNameAscAmountDesc;
import Application.Global;
import Application.Tender;
import Application.TenderGroup;
import Application.TotalRecord;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.event.*;


public class ViewPanel extends JPanel implements ActionListener {
    //Панель просмотра
    JTextField leftBorderInput; //левая граница диапазона
    JTextField rightBorderInput;  //правая граница диапазона
    JTextField textFielFilter; //фильтр
    public ViewPanel(){
        setLayout(new GridLayout(7,2,2,2));
        JButton btnAvgAmount = new JButton ("Средняя предлагаемая сумма");
        JButton btnAboveAvg = new JButton ("Число заявок, у которых цена выше среднего");
        JButton btnBetween = new JButton ("Число заявок в диапазоне");
        JButton btnTotal1 = new JButton ("Итог 1:  Минимальная сумма за работу");
        JButton btnTotal2 = new JButton ("Итог 2:  Число заявок на тендер");
        JButton btnAddFilter = new JButton ("Применить фильтр");
        JButton btnSortByCodeAndByAmount = new JButton ("Сортировать по предагаемой сумме и по названию работ");
        JButton btnSortByCipherAndAmount = new JButton ("Сортировать по шифру заказчика и предагаемой сумме");
        JButton btnShowAll = new JButton ("Вывести все");
        textFielFilter = new JTextField("");
        JLabel labelFilter = new JLabel("Введите фильтр для компаний",JLabel.CENTER);
        JLabel l2 = new JLabel("");
        JLabel labelBorders = new JLabel("Укажите границы диапазона",JLabel.CENTER);
        btnAvgAmount.setActionCommand("Avg");
        btnAboveAvg.setActionCommand("AboveAvg");
        btnBetween.setActionCommand("Between");
        btnTotal1.setActionCommand("Total1");
        btnTotal2.setActionCommand("Total2");
        btnAddFilter.setActionCommand("Filter");
        btnSortByCodeAndByAmount.setActionCommand("sortByCodeAndByAmount");
        btnSortByCipherAndAmount.setActionCommand("sortByCipherAndByAmount");
        btnShowAll.setActionCommand("All");
        leftBorderInput = new JTextField("",10);
        rightBorderInput = new JTextField("",10);
        JPanel p1 = new JPanel();
        p1.add(leftBorderInput); p1.add(rightBorderInput);
        add(labelFilter);
        add(l2);
        add(textFielFilter);
        add(btnAddFilter);
        add(btnAvgAmount);
        add(btnShowAll);
        add(labelBorders);
        add(btnAboveAvg);
        add(p1);
        add(btnBetween);
        add(btnSortByCodeAndByAmount);
        add(btnSortByCipherAndAmount);
        add(btnTotal1);
        add(btnTotal2);

        btnAvgAmount.addActionListener(this);
        btnAboveAvg.addActionListener(this);
        btnBetween.addActionListener(this);
        btnTotal1.addActionListener(this);
        btnTotal2.addActionListener(this);
        btnAddFilter.addActionListener(this);
        btnSortByCodeAndByAmount.addActionListener(this);
        btnSortByCipherAndAmount.addActionListener(this);
        btnShowAll.addActionListener(this);
    }
    private void showAvg(){
        MainFrame.msg.setText("   Запрос на выборку: выдать среднюю цену");
        JOptionPane.showMessageDialog(MainFrame.jFrame,
                String.format("Средняя предлагаемая сумма: %6.2f", Global.table.avgPrice()));
    }

    private void showAboveAvg(){
        MainFrame.msg.setText("   Запрос на выборку: выдать тендеры с ценой выше среднего");
        Global.updateJTable(Global.table.aboveAvgPrice().getTenders());
        MainFrame.jFrame.repaint();
        MainFrame.jFrame.pack();
        MainFrame.jFrame.setSize(800, 600);
    }

   private void showBetween(){
       double leftBorder;
       double rightBorder;

       try{//Контролируем исключения при преобразовании из String в число
           leftBorder = Double.parseDouble(leftBorderInput.getText());
           rightBorder = Double.parseDouble(rightBorderInput.getText());
       }//try 2
       catch (NumberFormatException  e){// обработчик исключения для try
           MainFrame.msg.setText("   Задайте правильно границы диапазона");
           return;}
       MainFrame.msg.setText(String.format(
               "   Запрос на выборку: выдать записи с ценой в диапазоне [%4.3f ,%4.3f]", leftBorder, rightBorder));
       Global.updateJTable(Global.table.betweenPrice(leftBorder,rightBorder).getTenders());
       MainFrame.jFrame.repaint();
       MainFrame.jFrame.pack();
       MainFrame.jFrame.setSize(800, 600);

   }

    private void showTotalNumbersOfTenders(){
        MainFrame.msg.setText("   Итоговый запрос на выборку");
        JOptionPane.showMessageDialog(MainFrame.jFrame, String.format("Общее число заявок на тендер: %5d", Global.table.getTenders().size()));
    };

   private void showTotal1(){

       List<TotalRecord> totalRecordList = Global.table.getMinAmounts();
       TotalDialog totalDialog = new TotalDialog (MainFrame.jFrame, totalRecordList);
       totalDialog.setVisible(true);

   }

   private void showFilter(){
       String filter = textFielFilter.getText();
       if (filter.equals("")) {
           MainFrame.msg.setText("     Введите фильтр");
           return;
       }
       MainFrame.msg.setText(String.format(
               "   Запрос на выборку: выдать записи с компанией, начинающейся с \"%s\"", filter));
       Global.updateJTable(Global.table.filterCompany(filter).getTenders());
       MainFrame.jFrame.repaint();
       MainFrame.jFrame.pack();
       MainFrame.jFrame.setSize(800, 600);
   }

   private void sortByNameAndByAmount(){
       MainFrame.msg.setText("   Запрос на выборку: выдать все записи таблицы с сортировкой предлагаемой сумме и названию услуг");
       Global.updateJTable(Global.table.sort(new CompNameAscAmountDesc()).getTenders());
       MainFrame.jFrame.repaint();
       MainFrame.jFrame.pack();
       MainFrame.jFrame.setSize(800, 600);
   }
   private void sortByCipherAndByAmount(){
           MainFrame.msg.setText("   Запрос на выборку: выдать все записи таблицы с сортировкой по шифру заказчика и предлагаемой сумме");
           Global.updateJTable(Global.table.sort(new CompCipherAscAmountDesc()).getTenders());
           MainFrame.jFrame.repaint();
           MainFrame.jFrame.pack();
           MainFrame.jFrame.setSize(800, 600);
       }

    private void showAll(){
        MainFrame.msg.setText("   Запрос на выборку: выдать все записи таблицы без сортировки");
        Global.updateJTable(Global.table.getTenders());
        MainFrame.jFrame.repaint();
        MainFrame.jFrame.pack();
        MainFrame.jFrame.setSize(800, 600);
    }

    public void actionPerformed (ActionEvent e){
        if ("Avg".equals(e.getActionCommand())) showAvg();
        else if("AboveAvg".equals(e.getActionCommand())) showAboveAvg();
        else if("Between".equals(e.getActionCommand())) showBetween();
        else if("Total1".equals(e.getActionCommand())) showTotal1();
        else if("Total2".equals(e.getActionCommand())) showTotalNumbersOfTenders();
        else if("Filter".equals(e.getActionCommand())) showFilter();
        else if("sortByNameAndByAmount".equals(e.getActionCommand())) sortByNameAndByAmount();
        else if("sortByCipherAndByAmount".equals(e.getActionCommand())) sortByCipherAndByAmount();

        else showAll();
    }
} 