package Application.GUI;

import Application.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class MenuBar{

     JMenuBar jMenuBar;
     JMenu fileMenu;
     JMenu editingMenu;
     JMenu referenceMenu;
     JMenuItem newItem;
     JMenuItem openItem;
     JMenuItem saveItem;
     JMenuItem saveAsItem;
     JMenuItem closeItem;
     JMenuItem startEditing;
     JMenuItem endEditing;
     JMenuItem about;
     JMenuItem description;

    public MenuBar(){

        jMenuBar = new JMenuBar();

        //Create 1st JMenu
        fileMenu = new JMenu("Файл");

        newItem = new JMenuItem("Новый");
        openItem = new JMenuItem("Открыть");
        saveItem = new JMenuItem("Сохранить");
        saveAsItem = new JMenuItem("Сохранить как");
        closeItem = new JMenuItem("Закрыть");

        fileMenu.add(newItem);
        fileMenu.add(openItem);
        fileMenu.add(saveItem);
        fileMenu.add(saveAsItem);
        fileMenu.add(closeItem);

        jMenuBar.add(fileMenu);

        //Create 2nd JMenu
        editingMenu = new JMenu("Редактирование");

        startEditing = new JMenuItem("Начать редактирование");
        endEditing = new JMenuItem("Закончить редактирование");

        editingMenu.add(startEditing);
        editingMenu.add(endEditing);

        jMenuBar.add(editingMenu);

        //Create 3rd JMenu
        referenceMenu = new JMenu("Справка");

        about = new JMenuItem("О программе");
        description = new JMenuItem("Описание ИС");

        referenceMenu.add(about);
        referenceMenu.add(description);

        jMenuBar.add(referenceMenu);

    }

}
