package Application;

public class TotalRecord {

    String codeOfTheCompany;
    String nameOfTheService;
    double minProposedAmount;

    public TotalRecord(){

        codeOfTheCompany = "";
        nameOfTheService = "";
        minProposedAmount = 0.0;

    }

    public TotalRecord(String codeOfTheCompany, String nameOfTheService, double minProposedAmount){

        this.codeOfTheCompany = codeOfTheCompany;
        this.nameOfTheService = nameOfTheService;
        this.minProposedAmount = minProposedAmount;

    }

    public String getCodeOfTheCompany() {
        return codeOfTheCompany;
    }

    public void setCodeOfTheCompany(String codeOfTheCompany) {
        this.codeOfTheCompany = codeOfTheCompany;
    }

    public String getNameOfTheService() {
        return nameOfTheService;
    }

    public void setNameOfTheService(String nameOfTheService) {
        this.nameOfTheService = nameOfTheService;
    }

    public double getMinProposedAmount() {
        return minProposedAmount;
    }

    public void setMinProposedAmount(double minProposedAmount) {
        this.minProposedAmount = minProposedAmount;
    }

    @Override
    public String toString() {
        return "TotalRecord{" +
                "codeOfTheCompany='" + codeOfTheCompany + '\'' +
                ", nameOfTheService='" + nameOfTheService + '\'' +
                ", minProposedAmount=" + minProposedAmount +
                '}';
    }

}
