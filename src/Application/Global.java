package Application;

import java.util.ArrayList;
import java.util.List;

public class Global {

    public static TenderGroup table;

    public static List<Tender> tenders;

    public static TenderTableModel tableModel;

    public static void updateJTable(List<Tender> res){
        tenders.clear();
        tenders.addAll(res);
        tableModel.fireTableDataChanged();
    }

}
