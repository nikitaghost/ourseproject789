package Application;

public class Tender implements Comparable <Tender>{

    private String codeOfTheCompany;
    private String enterpriseCode;
    private String nameOfTheService;
    private double proposedAmount;

    public Tender(){
        codeOfTheCompany = "";
        enterpriseCode = "";
        nameOfTheService = "";
        proposedAmount = 0.;
    }

    public Tender(String codeOfTheCompany, String enterpriseCode, String nameOfTheService, double proposedAmount) {
        this.codeOfTheCompany = codeOfTheCompany;
        this.enterpriseCode = enterpriseCode;
        this.nameOfTheService = nameOfTheService;
        this.proposedAmount = proposedAmount;
    }

    public String getCodeOfTheCompany() {
        return codeOfTheCompany;
    }

    public void setCodeOfTheCompany(String codeOfTheCompany) {
        this.codeOfTheCompany = codeOfTheCompany;
    }

    public String getEnterpriseCode() {
        return enterpriseCode;
    }

    public void setEnterpriseCode(String enterpriseCode) {
        this.enterpriseCode = enterpriseCode;
    }

    public String getNameOfTheService() {
        return nameOfTheService;
    }

    public void setNameOfTheService(String nameOfTheService) {
        this.nameOfTheService = nameOfTheService;
    }

    public double getProposedAmount() {
        return proposedAmount;
    }

    public void setProposedAmount(double proposedAmount) {
        this.proposedAmount = proposedAmount;
    }

    @Override
    public String toString() {
        return "Application.Tender{" +
                "codeOfTheCompany='" + codeOfTheCompany + '\'' +
                ", enterpriseCode='" + enterpriseCode + '\'' +
                ", nameOfTheService='" + nameOfTheService + '\'' +
                ", proposedAmount=" + proposedAmount +
                '}';
    }

    @Override
    public int compareTo(Tender o) {

        int code = codeOfTheCompany.compareTo(o.getCodeOfTheCompany());
        if (code < 0){
            return -1;
        }
        else if (code > 0){
            return 1;
        }

        code = enterpriseCode.compareTo(o.getEnterpriseCode());
        if (code < 0){
            return -1;
        }
        else if (code > 0){
            return 1;
        }

        return 0;

    }
}
